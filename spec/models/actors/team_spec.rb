require 'rails_helper'

describe Team do
  describe '#program_stars_at' do
    before do
      @time_program_starts_at = Time.now + 1.day
      @team = FactoryGirl.create(:team, program_starts_at: @time_program_starts_at)
    end

    it 'always stars at the beginning of the week' do
      expect(@team.program_starts_at).to eq(@time_program_starts_at.beginning_of_week)
    end
  end

  describe 'validations' do
    before do
      @team = FactoryGirl.build(:team, program_starts_at: Time.now - 1.day)
    end

    it 'validates that program_starts_at is greater than its creation time' do

      expect(@team).to_not be_valid
    end

    it 'adds a relevant error message for attribute #program_starts_at' do
      @team.save

      expect(@team.errors).to have_key(:program_starts_at)
    end
  end
end
