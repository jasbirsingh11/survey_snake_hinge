require 'rails_helper'

describe SurveyObligation do
  let(:user) { FactoryGirl.create(:user_with_team) }
  let(:survey) { user.survey_obligations.first }

  before do
    @survey_definition =
      FactoryGirl.create(:survey_definition, week_indices: [0])
  end

  describe '.due_this_week' do
    before do
      survey.update(due_at: Date.today)
    end

    it 'gives the list of obligations due this week' do
      expect(described_class.due_this_week).to include(survey)
    end
  end

  describe '.submittable_now' do
    before do
      survey.update(submitted_at: nil, due_at: Date.today - 1, expires_at: Date.today + 1)
    end

    it 'lists surveys which are due for submission' do
      expect(described_class.submittable_now).to include(survey)
    end

    it 'does not return submitted surveys' do
      survey.update(submitted_at: Time.now)

      expect(described_class.submittable_now).to be_empty
    end
  end

  describe '.missed' do
    it 'does not list surveys that have expired' do
      survey.update(submitted_at: nil, expires_at: Date.today)

      expect(described_class.missed).to be_empty
    end

    it 'lists all of the surveys meant to be submitted but without submission' do
      survey.update(submitted_at: nil, expires_at: Date.today + 1)

      expect(described_class.missed).to include(survey)
    end
  end

  describe '.obligations_due' do
    before do
      FactoryGirl.create(:survey_definition, week_indices: [1, 2, 3])
    end

    it 'lists the survey obligations due on a given week' do
      obligations = @survey_definition.survey_obligations

      expect(described_class.obligations_due(week_index: 0)).to eq(obligations)
    end
  end

  describe '.search_by_survey_name' do
    it 'returns an obligation which is next due for a survey' do
      survey.update(due_at: Date.today)

      expect(described_class.search_by_survey_name(@survey_definition.name)).to eq(survey)
    end
  end
end
