require 'rails_helper'

describe ApiController do
  context 'no access_token is provided' do
    before { get :authorised }
    it 'returns 401 HTTP Unauthorised status' do

      expect(response.status).to eq(401)
    end

    it 'returns the right status message' do
      expect(response.body).to match(/Unauthorised/)
    end
  end

  context 'access_token is provided' do
    before { get :authorised }
    it 'returns 200 HTTP Unauthorised status' do

      expect(response.status).to eq(401)
    end

    it 'returns the right status message' do
      expect(response.body).to match(/authorised/)
    end
  end

end
