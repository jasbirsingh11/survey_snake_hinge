require './lib/format_validator'

describe SurveySnake::FormatValidator do
  subject do
    Object.new.tap do |o|
      o.extend described_class
    end
  end

  it 'validates input based on a given format' do
    format = [{ name: 'name1', required: true, type: 'boolean'}]
    submission = {'name1' => 'true'}

    expect(subject.validate_format(format, submission)).to eq(true)
  end

  it 'validates input based on a given format' do
    format = [{ name: 'name1', required: true, type: 'boolean'}]
    submission = {}

    expect(subject.validate_format(format, submission)).to eq(false)
  end

  it 'validates input based on a given format' do
    format = [{ name: 'name1', required: true, type: 'integer'}]
    submission = {'name1' => '1'}

    expect(subject.validate_format(format, submission)).to eq(true)
  end

  it 'validates input based on a given format' do
    format = [{ name: 'name1', required: true, type: 'integer'}]
    submission = {'name1' => 'string'}

    expect(subject.validate_format(format, submission)).to eq(false)
  end
end

