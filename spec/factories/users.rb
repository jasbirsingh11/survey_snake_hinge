FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    token { Faker::Hipster.word }

    factory(:user_with_team) do
      before(:create) do |user|
        user.team = FactoryGirl.create(:team)
      end
    end
  end
end
