require "active_support/hash_with_indifferent_access"
module SurveySnake
  module FormatValidator
    def validate_format(format, submission)
      format = format.map(&:with_indifferent_access)
      submission = submission.with_indifferent_access

      format.all? do |hash|
        if submission[hash[:name]]
          validate_type(hash[:type], submission[hash[:name]])
        else hash[:required] && !submission[hash[:name]]
          false
        end
      end
    end

    private

    def validate_type(type, input)
      case type
      when 'integer'
        validate_integer(input)
      when 'boolean'
        validate_boolean(input)
      else
        true
      end
    end

    def validate_integer(input)
      Integer(input) rescue false
    end

    def validate_boolean(input)
      %w(true false).include?(input)
    end
  end
end
