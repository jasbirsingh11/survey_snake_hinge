class SurveysController < ApiController
  def show
    obligation = user_obligations.find_by_id(params[:id]) ||
                  user_obligations.search_by_survey_name(params[:id])

    render json: obligation
  end

  def due_this_week
    render json: user_obligations.due_this_week
  end

  def submittable_now
    render json: user_obligations.submittable_now
  end

  def missed
    render json: user_obligations.missed
  end

  def due
    render json: user_obligations.obligations_due(week_index: params[:week_index])
  end

  def submit
    obligation = user_obligations.search_by_survey_name(params[:name])

    if obligation.update(submission: survey_submission)
      render status: 200, json: ''
    else
      render status: 422, json: 'Invalid format'
    end
  end

  private

  def survey_submission
    params.require(:body)
  end

  def user_obligations
    current_user.survey_obligations
  end
end

=begin
1) All responses sent back to the requester in JSON
2) All actions that return lists must support pagination specified in the params (i.e items_per_page=y)
3) Users are identified & authenticated against the "token" column. You can choose whether you expect the value in the params or headers
4) Obviously, since all surveys obligations are user specific, all the information you query here must be specific to the current user
5) Unauthorized access should yield an error response in JSON
6) Any survey obligation/definition returned should have all fields EXCEPT user_id (if applicable), updated_at, and created_at
=end
