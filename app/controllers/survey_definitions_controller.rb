class SurveyDefinitionsController < ApiController
  def show
    definition = SurveyDefinition.find_by_id(params[:id]) ||
                    SurveyDefinition.find_by_name(params[:id])
    render json: definition
  end

  def due_this_week
    render json: user_obligations.includes(:survey_definition).due_this_week.map(&:survey_definition).uniq
  end

  def submittable_now
    render json: user_obligations.includes(:survey_definition).submittable_now.map(&:survey_definition).uniq
  end

  def missed
    render json: user_obligations.includes(:survey_definition).missed.map(&:survey_definition).uniq
  end

  def due
    render json: user_obligations.obligations_due(week_index: params[:week_index]).map(&:survey_definition).uniq
  end

  private

  def user_obligations
    current_user.survey_obligations
  end
end
